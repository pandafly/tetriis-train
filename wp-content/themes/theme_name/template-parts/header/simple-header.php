<header class="site-header">
    <div class="site-header__fixed">
        <div class="max-width-full container-fluid">
            <div class="site-header__wrapper">
                <div class="site-header__wrapper__logo">
                    <?php if (function_exists('the_custom_logo') && has_custom_logo()) the_custom_logo(); ?>
                </div>
                <div class="site-header__wrapper__menu">
                    <?php $args['menu']->insertMenu(); ?>
                </div>
                <div class="site-header__wrapper__mobile"></div>
            </div>
        </div>
    </div>
    <div class="mob-menu">
        <?php get_template_part('template-parts/header/mob', 'menu', ['menu' => $args['menu']]) ?>
    </div>
</header>