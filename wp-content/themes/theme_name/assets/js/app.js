document.addEventListener('DOMContentLoaded', function () {

    /// Burger menu ///
    (() => {
        var menuBtn = document.querySelector('.site-header__wrapper__mobile');
        var menu = document.querySelector('.mob-menu');
        var closeBtn = document.querySelector('.mob-menu__wrapper__mobile');

        if (menuBtn) {
            menuBtn.addEventListener('click', () => {
                menu.classList.add('open')
            })
        }
        if (closeBtn) {
            closeBtn.addEventListener('click', () => {
                menu.classList.remove('open')
            })
        }
    })();

    // Hidden Menu

    var headerMenu = document.querySelector('.menu-header');
    var body = document.querySelector('body');
    document.getElementById("menu-btn").addEventListener("click", function () {
        headerMenu.classList.add('open')
        body.classList.add('active-menu')
    })
    document.getElementById("menu-close-btn").addEventListener("click", function () {
        headerMenu.classList.remove('open')
        body.classList.remove('active-menu')

    })

})