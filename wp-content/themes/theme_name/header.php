<?php

use ThemeOptions\RegisterMenus;
$registerMainHeader = new RegisterMenus('MainHeader', 'THEME_NAME');


?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title(); ?></title>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open();
//get_template_part('template-parts/header/simple', 'header', ['menu' => $registerMainHeader]);
get_template_part('template-parts/header/hidden', 'header', ['menu' => $registerMainHeader]);

?>
